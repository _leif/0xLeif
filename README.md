# Hello there ✌️

I'm Zach, I've been making iOS apps since 2013. I am focusing on anything Swift! 

<img align="right" src="https://github-readme-stats.vercel.app/api?username=0xLeif&show_icons=true&icon_color=f05139&text_color=000000&bg_color=ffffff&hide_title=true&title_color=ffac45&count_private=true" />

- 🔭 I’m currently working on [ServerDriven](https://github.com/ServerDriven)
- 🌱 I’m currently learning [Vapor](https://vapor.codes) and [SwiftUI](https://developer.apple.com/documentation/swiftui)
- 💬 Ask me about [Swift](https://github.com/0xSwift) 🧡
- 📫 How to reach me: [JOIN ONELEIF](https://discord.com/invite/tv9UdJK)

#### Check Out My...
- [iOS Apps](https://apps.apple.com/lb/developer/zach-eriksen/id851997363)
- [Work Blogs](https://www.clientresourcesinc.com/author/zeriksen/)
- [Medium](https://medium.com/@0xLeif)


<a href="https://github.com/0xLeif/SwiftUIKit">
  <img src="https://github-readme-stats.vercel.app/api/pin/?username=0xLeif&repo=SwiftUIKit" height=130 />
</a>
<a href="https://github.com/0xLeif/Later">
  <img src="https://github-readme-stats.vercel.app/api/pin/?username=0xLeif&repo=Later" height=130 />
</a>
<a href="https://github.com/0xLeif/E.num">
  <img src="https://github-readme-stats.vercel.app/api/pin/?username=0xLeif&repo=E.num" height=130 />
</a>
<a href="https://github.com/0xLeif/AwesomeArticles">
  <img src="https://github-readme-stats.vercel.app/api/pin/?username=0xLeif&repo=AwesomeArticles" height=130 />
</a>
